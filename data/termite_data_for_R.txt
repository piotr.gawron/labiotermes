SUBSTRATE	FAMILY	F	M	P1	P3	P4	P5
Cellulose	GH9	3	3	3	3	3	2
Cellulose	GH94						
Cellulose	GH128						
Chitin and peptidoglycan	GH18	10	10	10	9	10	9
Chitin and peptidoglycan	GH19						
Chitin and peptidoglycan	GH20	4	4	4	4	4	4
Hemicellulose	GH2	2	2	2	2	2	2
Hemicellulose	GH4						
Hemicellulose	GH11						
Hemicellulose	GH13	7	7	7	7	7	7
Hemicellulose	GH14						
Hemicellulose	GH15						
Hemicellulose	GH16	2	2	3	3	3	3
Hemicellulose	GH27						
Hemicellulose	GH29	1	1	1	1	1	1
Hemicellulose	GH31	3	2	3	2	3	3
Hemicellulose	GH35	3	2	3	1	3	3
Hemicellulose	GH38	3	1	3	3	3	3
Hemicellulose	GH42						
Hemicellulose	GH43						
Hemicellulose	GH47	5	2	4	4	4	5
Hemicellulose	GH53						
Hemicellulose	GH54						
Hemicellulose	GH55						
Hemicellulose	GH57						
Hemicellulose	GH63	1		1	1	1	1
Hemicellulose	GH67						
Hemicellulose	GH76						
Hemicellulose	GH77						
Hemicellulose	GH92						
Hemicellulose	GH93						
Hemicellulose	GH95						
Hemicellulose	GH97						
Hemicellulose	GH99	1	1	1	1	1	1
Hemicellulose	GH110						
Hemicellulose	GH113						
Hemicellulose	GH115						
Hemicellulose	GH120						
Hemicellulose	GH127						
Hemicellulose	GH130						
Hemicellulose	GH133						
Hemicellulose	GH137						
Hemicellulose	GH141						
Hemicellulose	GH144						
Hemicellulose and cellulose	GH1	11	10	11	10	11	11
Hemicellulose and cellulose	GH3						
Hemicellulose and cellulose	GH5						
Hemicellulose and cellulose	GH8						
Hemicellulose and cellulose	GH10						
Hemicellulose and cellulose	GH26						
Hemicellulose and cellulose	GH30	3	3	3	2	4	4
Hemicellulose and cellulose	GH39						
Hemicellulose and cellulose	GH45						
Hemicellulose and cellulose	GH51						
Hemicellulose and cellulose	GH74						
Hemicellulose and cellulose	GH116						
other	GH28						
other	GH37	5	4	5	4	4	5
other	GH50						
other	GH56	1		1	1		1
other	GH65						
other	GH78						
other	GH79	1	1	1	1	1	1
other	GH88						
other	GH89	1	1	1	1	1	1
other	GH101						
other	GH105						
other	GH106						
other	GH109						
other	GH112						
other	GH114						
other	GH123						
other	GH136						
other	GH140						
Peptidoglycan	GH22	2	2	2	2	2	2
Peptidoglycan	GH23						
Peptidoglycan	GH24						
Peptidoglycan	GH25						
Peptidoglycan	GH73						
Peptidoglycan	GH84						
Peptidoglycan	GH85						
Peptidoglycan	GH102						
Peptidoglycan	GH103						
Peptidoglycan	GH104						
Peptidoglycan	GH108						
